<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance"
	prefix="layout"%>
<layout:extends name="base">
	<layout:put block="contents" type="REPLACE">
		<h2 class="ui center aligned icon header">
			<i class="circular opencart icon"></i> Iniciar sesión
		</h2>
		<div class="ui container segment">
			<div class="ui two column middle aligned very relaxed stackable grid">
				<div class="column">
					<form class="ui form" action="/tiendapolo/login" method="post">
						<div class="field">
							<label>Usuario</label>
							<div class="ui left icon input">
								<input type="text" placeholder="Username" name="usuario">
								<i class="user icon"></i>
							</div>
						</div>
						<div class="field">
							<label>Contraseña</label>
							<div class="ui left icon input">
								<input type="password" name="password" placeholder="Contraseña"> <i
									class="lock icon"></i>
							</div>
						</div>
						<button class="ui blue submit button">Iniciar sesión</button>
					</form>
				</div>
				<div class="center aligned column">
					<a class="ui big green labeled icon button"
						href="/tiendapolo/signup.jsp"> <i class="signup icon"></i>
						Crear cuenta
					</a>
				</div>
			</div>
		</div>
	</layout:put>
</layout:extends>