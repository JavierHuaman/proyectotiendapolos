
var $modalProducts = $('.ui.modal.products');
var $color_container = $('.swatch');
var $modalArtWork = $('.ui.modal.mdl-art-work');
var $designArea = $('.design-area');
var $layouts = $('.layouts');
var $modalCreateText = $('.ui.modal.mdl-create-text');
var $currentLayouts = [];
var $currentTotal = 0;

function updateLayouts() {
  $layouts.html("<h5>Agregados</h5>");
  if (!$currentLayouts.length) {
	  $layouts.hide();
	  return;
  }
  $layouts.show();
  var li = $('<div class="ui middle aligned divided list" />');
  for(var kk in $currentLayouts) {
    var layout = $currentLayouts[kk];
    var item = $('<div class="item" />');
    item.append('<img class="ui avatar image" src="images/2/' + layout.image + '" />');
    item.append($('<div class="content"/>').html('<a class="header">' + layout.name + ' ($' + layout.price + ')</a>'));
    li.append(item);
  }
  $layouts.append(li);
  $('.money-total').html('$ ' + $currentTotal);
  DETALLE['precio'] = $currentTotal;
}


function Init () {
  $modalProducts
    .modal('attach events', '#pickProduct', 'show')
    .modal('show');
    
  $modalArtWork
  	.modal('attach events', '#pickArtWork', 'show');

  $modalCreateText.modal({
    onApprove : function() {
      var span  = $('<span />');
      span.html($('#txt-create-text').val());
      span.draggable();
      $designArea.append(span);
    }    
  }).modal('attach events', '#pickCreateText', 'show');

  $(document).on('click', 'span.bg-colors', function() {
    $.each($('.bg-colors'), function() {
      $(this).removeClass('selected');
    });
    var color = $(this).attr('rel');
    $(this).addClass('selected');
    $('#mask').css({ 'background-color':  color });
    DETALLE['colores'] = color;
  });
  
  $(document).on('click', 'a.pickArtWork', function() {
	var code = $(this).attr('data-code');
    var artwork = ARTWORK[code];
    var span = $('<span />').html('<img src="images/2/' + artwork.image + '"/>');
    span.draggable();
    $designArea.append(span);
    $modalArtWork.modal('hide');
    $currentLayouts.push(artwork);
    $currentTotal += artwork.price;
    DETALLE['detalle'].push(code); 
    updateLayouts();
    alert('Agregado');
  });

  $(document).on('click', 'a.pickName', function() {
	var code = $(this).attr('data-code');
    var shirt = SHIRTS[code];
    $('#mask-content').html("<img id='mask' width='80%' src='images/1/" + shirt.image + "'/>");
    $modalProducts.modal('hide');
    $color_container.empty();
    for(var ii in shirt.colors) {
      var color = shirt.colors[ii];
      $color_container.append('<div class="four wide column">' +
                    '<span rel="' + color + '" class="bg-colors" style="background-color:' + color + '"></span></div>');

    }
    $currentTotal = shirt.price;
    DETALLE['idProducto'] = code;
    DETALLE['descripcion'] = shirt.name;
    DETALLE['idTipoProducto'] = '3';
    DETALLE['precio'] = $currentTotal;
    DETALLE['detalle'] = [];
    $currentLayouts = [];
    updateLayouts();
    $designArea.html('');
    
    $('.money-total').html('$ ' + $currentTotal);
  });
  
  $('#btnAgregarCarrito').click(function() {
	  
	  if (DETALLE.detalle.length) {
		  $designArea.addClass('bordernone');
		  html2canvas($('#content-to-image'), {
		    onrendered: function (canvas) {
				var imagedata = canvas.toDataURL('image/png');
				var imgdata = imagedata.replace(/^data:image\/(png|jpg);base64,/, "");
				  
				DETALLE['detalle'] = DETALLE['detalle'].join(';');
				DETALLE['operacion'] = 'agregar';
				DETALLE['imgdata'] = imgdata;
				
				$.ajax({
					type: 'post',
					url: '/tiendapolo/carritocompras',
					data: DETALLE,
					success: function(data) {
					  window.location.href = "carritoCompras.jsp";
					}
				});
		    }
		  });
	  }
  });
  
  
};