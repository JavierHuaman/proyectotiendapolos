<%@page import="com.tiendapolo.entidad.Producto"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance"
	prefix="layout"%>
<%
	List<Producto> polos = (List<Producto>) request.getAttribute("polos");
	List<Producto> artworks = (List<Producto>) request.getAttribute("artworks");
%>
<layout:extends name="base">
	<layout:put block="contents" type="REPLACE">
		<div class="ui container segment">
			<div class="ui grid">
				<div class="four wide column">
					<div class="ui large vertical labeled icon menu">
						<a class="item" id="pickProduct"> <i class="gamepad icon"></i>
							Elegir productos
						</a> <a class="item" id="pickCreateText"> <i
							class="video camera icon"></i> Agregar Texto
						</a> <a class="item" id="pickArtWork"> <i class="video play icon"></i>
							Agregar imagen
						</a>
					</div>
					<div class="ui segment layouts" style="display: none;"></div>
				</div>
				<div class="eight wide centered column" id="content-to-image">
					<div class="" id="mask-content"></div>
					<div class="design-area" style="top: 120px; left: 128px;"></div>
				</div>
				<div class="four wide column">
					<h5 class="ui top attached header">Elegir color :</h5>
					<div class="ui attached segment">
						<div class="ui padded grid swatch"></div>
					</div>
					<div class="ui attached segment">
						<h5 class="ui header">Costo base :</h5>
						<p class="money-total"></p>
					</div>
					<div class="ui attached segment">
						<button type="button" class="ui small teal button"
							id="btnAgregarCarrito">Agregar a carrito</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal productos -->
		<div class="ui modal products">
			<i class="close icon"></i>
			<div class="header">Escoga un producto</div>
			<div class="content">
				<div class="ui padded grid">
					<c:forEach items="${polos}" var="polo">
						<div class="five wide column">
							<h5 class="ui top attached header">${polo.descripcion}</h5>
							<div class="ui attached segment">
								<img src="images/1/${polo.imagen}" width="100%" />
							</div>
							<div class="ui attached segment">
								<a class="ui small teal button pickName"
									data-code="${polo.idProducto}">Seleccionar</a>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="ui modal mdl-create-text">
			<i class="close icon"></i>
			<div class="header">Ingrese texto</div>
			<div class="content">
				<div class="ui input">
					<input type="text" id="txt-create-text" placeholder="Ingrese texto"
						value="Prueba Diseño!!!">
				</div>
			</div>
			<div class="actions">
				<div class="ui approve button">Agregar</div>
			</div>
		</div>
		<!-- Modal logo -->
		<div class="ui modal mdl-art-work">
			<i class="close icon"></i>
			<div class="header">Agregar logo</div>
			<div class="content">
				<div class="ui padded grid">
					<c:forEach items="${artworks}" var="artwork">
						<div class="five wide column">
							<div class="ui attached segment">
								<img src="images/2/${artwork.imagen}" width="100%" />
							</div>
							<div class="ui attached segment">
								<a class="ui small teal button pickArtWork"
									data-code="${artwork.idProducto}">Seleccionar</a>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>

	</layout:put>
	<layout:put block="scripts">
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="js/html2canvas.js"></script>
		<script type="text/javascript" src="js/init.js"></script>
		<script type="text/javascript">
  	var DETALLE = {};
  	var SHIRTS = {};
	<c:forEach items="${polos}" var="polo">
		SHIRTS['${polo.idProducto}'] = {
			name: '${polo.descripcion}',
			price: ${polo.precio},
			image: '${polo.imagen}',
			colors: '${polo.colores}'.split(';')
		};
	</c:forEach> 
  	var ARTWORK = {};
	<c:forEach items="${artworks}" var="artw">
		ARTWORK['${artw.idProducto}'] = {
			name: '${artw.descripcion}',
			price: ${artw.precio},
			image: '${artw.imagen}'
		};
	</c:forEach> 
  
    Init();
  </script>
	</layout:put>
</layout:extends>