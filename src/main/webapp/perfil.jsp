<%@page import="com.tiendapolo.service.PoloService"%>
<%@page import="com.tiendapolo.entidad.PedidoHasProducto"%>
<%@page import="java.util.List"%>
<%@page import="com.tiendapolo.service.PedidoService"%>
<%@page import="com.tiendapolo.entidad.Pedido"%>
<%@page import="com.tiendapolo.entidad.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance"
	prefix="layout"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	PedidoService pedidoService = new PedidoService();
	PoloService poloService = new PoloService();
	Usuario usuario = (Usuario) session.getAttribute("usuario");
	List<Pedido> pedidos = pedidoService.pedidoXusuario(usuario.getIdUsuario());

	for (Pedido ped : pedidos) {
		ped.setPedidoHasProductos(pedidoService.detallepedidoXpedido(ped.getIdPedido()));
		for (PedidoHasProducto pHp : ped.getPedidoHasProductos()) {
			pHp.getProducto()
					.setProductos(poloService.detalleproductoXproducto(pHp.getProducto().getIdProducto()));
		}
	}
	usuario.setPedidos(pedidos);
%>
<layout:extends name="base">
	<layout:put block="contents" type="REPLACE">
		<h2 class="ui center aligned icon header">
			<i class="circular opencart icon"></i> Perfil
		</h2>

		<div class="ui two columns grid container">
			<div class="column">
				<div class="ui segment">
					<div class="ui list">
						<div class="item">
							<i class="male icon"></i>
							<div class="content">${usuario.nombres}</div>
						</div>
						<div class="item">
							<i class="phone icon"></i>
							<div class="content">${usuario.telefono}</div>
						</div>
						<div class="item">
							<i class="mail icon"></i>
							<div class="content">
								<a href="#">${usuario.email}</a>
							</div>
						</div>
						<div class="item">
							<i class="users icon"></i>
							<div class="content">${usuario.usuario}</div>
						</div>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="ui segment">
					<h5 class="ui header">Pedidos:</h5>
					<c:if test="${not empty usuario.pedidos}">
						<div class="ui styled fluid accordion">
							<c:forEach items="${usuario.pedidos}" var="ped">
								<div class="title">
									<i class="dropdown icon"></i> PED-${ped.idPedido}
								</div>
								<div class="content">
									<div>
										<c:forEach items="${ped.pedidoHasProductos}" var="pHp">
											<div class="ui warning message"
												id="pHp${pHp.idPedidoProducto}" style="margin-bottom: 0px;">
												<div class="header pHpHeader" style="cursor: pointer;">
													${pHp.producto.descripcion} - ${pHp.producto.precio}</div>
											</div>
											<c:set var="exists" value="false" />
											<table
												class="ui celled table pHp${pHp.idPedidoProducto} allpHp"
												style="display: none;">
												<thead>
													<tr>
														<th>Imagen</th>
														<th>Diseño</th>
														<th>Descripción</th>
														<th>Precio</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${pHp.producto.productos}" var="dtlProd">
														<tr>
															<c:if test="${!exists}">
																<td rowspan="${fn:length(pHp.producto.productos)}">
																	<img src="upload/${pHp.producto.imagen}" width="100%" />
																</td>
															</c:if>
															<c:set var="exists" value="true" />
															<td style="text-align: center;"><img
																src="images/2/${dtlProd.imagen}" width="35%" /></td>
															<td>${dtlProd.descripcion}</td>
															<td>${dtlProd.precio}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:forEach>
									</div>
								</div>
							</c:forEach>
						</div>

					</c:if>
					<c:if test="${empty usuario.pedidos}">
						<h2 class="ui header">El usuario no tiene pedidos.</h2>
					</c:if>
				</div>
			</div>
		</div>
	</layout:put>
	<layout:put block="scripts">
		<script>
			$('.accordion').accordion();
			$('.pHpHeader').click(function() {
				var idParent = $(this).parent().attr('id');
				console.log(idParent);
				$('.allpHp').hide();
				$('.' + idParent).show();
			});
		</script>
	</layout:put>
</layout:extends>