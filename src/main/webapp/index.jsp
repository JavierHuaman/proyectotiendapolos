<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance"
	prefix="layout"%>
<layout:extends name="base">
	<layout:put block="contents" type="REPLACE">
		<h2 class="ui center aligned icon header">
			<i class="circular opencart icon"></i> Tienda Online
		</h2>
	</layout:put>
</layout:extends>