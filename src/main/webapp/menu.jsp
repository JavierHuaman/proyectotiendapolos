<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="ui inverted fixed menu">
	<a class="item" href="/tiendapolo/"> Inicio </a> <a class="item"
		href="/tiendapolo/polos"> Polos </a>
	<div class="right menu">
		<a class="ui item" href="/tiendapolo/carritoCompras.jsp">
			Carrito de compras </a>
		<c:if test="${empty sessionScope.usuario}">
			<a class="ui item" href="/tiendapolo/signin.jsp">Iniciar sesión</a>
			<a class="ui item" href="/tiendapolo/signup.jsp">Registrarse</a>
		</c:if>
		<c:if test="${not empty sessionScope.usuario}">
			<div class="ui dropdown item">
				${sessionScope.usuario.nombres } <i class="dropdown icon"></i>
				<div class="menu">
					<a class="item" href="/tiendapolo/perfil.jsp">Perfil</a>
					<div class="divider"></div>
					<a class="item" href="/tiendapolo/logout">Cerrar sesión</a>
				</div>
			</div>
		</c:if>
	</div>
</div>
