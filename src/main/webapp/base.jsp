<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Tienda polos</title>
<link rel="stylesheet" href="css/semantic.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="ui grid container" style="margin-top: 3em;">
		<jsp:include page="menu.jsp" />
		<layout:block name="contents">
			<h2 class="ui center aligned icon header">
				<i class="circular opencart icon"></i> Tienda Online
			</h2>		
		</layout:block>
	</div>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/semantic.js"></script>
	<script type="text/javascript">
		$('.ui.dropdown').dropdown();
	</script>
	<layout:block name="scripts"></layout:block>
</body>
</html>