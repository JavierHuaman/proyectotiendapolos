<%@page import="com.tiendapolo.entidad.Producto"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance"
	prefix="layout"%>
<layout:extends name="base">
	<layout:put block="contents" type="REPLACE">
		<div class="ui container segment">
			<h1 class="ui header">Carrito de compras</h1>
			<c:if test="${not empty sessionScope.productos}">
				<div class="ui grid">
					<div class="ui ten wide column">
						<div class="ui styled fluid accordion">
							<c:forEach items="${sessionScope.productos}" var="prod">
								<div class="title">
									<i class="dropdown icon"></i> ${prod.descripcion} -
									${prod.precio} <a class="ui right floated mini teal button"
										href="/tiendapolo/carritocompras?operacion=eliminar&idProd=${prod.idProducto}"
										style="margin-top: -4px;">Eliminar</a>
								</div>
								<div class="content">
									<div class="ui grid">
										<div class="ui eight wide column">
											<a class="img-carrito" data-ref="${prod.imagen}"> <img
												src="upload/${prod.imagen}" width="100%" />
											</a>
										</div>
										<div class="ui eight wide column">
											<table class="ui celled table">
												<thead>
													<tr>
														<th>Imagen</th>
														<th>Descripción</th>
														<th>Precio</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${prod.productos}" var="dtlProd">
														<tr>
															<td style="text-align: center;"><img
																src="images/2/${dtlProd.imagen}" width="35%" /></td>
															<td>${dtlProd.descripcion}</td>
															<td>${dtlProd.precio}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
					<div class="ui six wide column">
						<table class="ui very basic table">
							<tbody>
								<tr>
									<td><b>Total : </b></td>
									<td>${sessionScope.total}</td>
								</tr>
								<tr>
									<td><a class="ui tiny grey basic button"
										href="/tiendapolo/polos"> <i class="shop icon"></i> Seguir
											comprando
									</a></td>
									<td><a class="ui tiny green button"
										href="/tiendapolo/pedido"> <i class="shop icon"></i>
											Generar
									</a></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</c:if>
			<c:if test="${empty sessionScope.productos}">
				<h2 class="ui header">El carrito esta vacio.</h2>
			</c:if>
		</div>
		<div class="ui modal" id="modalImagen">
			<i class="close icon"></i>
			<div class="header">Imagen detalle</div>
			<div class="image content">
				<img class="image">
			</div>
		</div>
	</layout:put>
	<layout:put block="scripts">
		<script type="text/javascript">
			$('#modalImagen').modal('attach events', '.img-carrito', 'show');
			$('.img-carrito').click(
					function() {
						$('#modalImagen').find('.image').attr('src',
								'upload/' + $(this).attr('data-ref'));
					});
			$('.ui.accordion').accordion();
		</script>

	</layout:put>
</layout:extends>