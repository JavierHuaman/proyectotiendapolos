package com.tiendapolo.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.tiendapolo.entidad.Producto;
import com.tiendapolo.entidad.ProductoHasProducto;

public class PoloService {
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("tiendapolos");
	EntityManager manager = emf.createEntityManager();
	
	
	@SuppressWarnings("unchecked")
	public List<Producto> productoXtipo(int idTipo) {
		List<Producto> productos = new ArrayList<Producto>();		
		try {
			Query q = null;
			String query = "SELECT u FROM Producto u where u.tipoProducto.idTipoProducto = ?1";
			q = manager.createQuery(query);
			q.setParameter(1, idTipo);
			productos = (List<Producto>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return productos;
		
	}
	
	public Producto buscarXid(int idProd) {
		return manager.find(Producto.class, idProd);
	}
	
	public boolean registrar(Producto producto) {
		boolean guardado = false;
		//GOOD cause readable
		EntityTransaction tx = manager.getTransaction();
		try {
			tx.begin();
			manager.persist(producto);
			tx.commit();
			guardado = true;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
		return guardado;
	}
	
	public boolean registrarDetalleProducto(ProductoHasProducto productoHas) {
		boolean guardado = false;
		EntityTransaction tx = manager.getTransaction();
		try {
			tx.begin();
			manager.persist(productoHas);
			tx.commit();
			guardado = true;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
		return guardado;
	}
	

	@SuppressWarnings("unchecked")
	public List<Producto> detalleproductoXproducto(int idProducto) {
		List<Producto> productos = new ArrayList<Producto>();		
		try {
			Query q = null;
			String query = "SELECT u.producto2 FROM ProductoHasProducto u where u.producto1.idProducto = ?1";
			q = manager.createQuery(query);
			q.setParameter(1, idProducto);
			productos = (List<Producto>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return productos;
		
	}
	
}
