package com.tiendapolo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.tiendapolo.entidad.Usuario;

public class UsuarioService {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("tiendapolos");
	EntityManager manager = emf.createEntityManager();
	
	
	public boolean registrar(Usuario usuario) {
		boolean guardado = false;
		//GOOD cause readable
		EntityTransaction tx = manager.getTransaction();
		try {
			tx.begin();
			manager.persist(usuario);
			manager.flush();
			tx.commit();
			guardado = true;
		} catch (Exception e) {
			tx.rollback();
		}
		return guardado;
	}
	
	@SuppressWarnings("unchecked")
	public Usuario iniciarSesion(String username, String password) {
		Usuario usuario = null;	
		try {
			Query q = null;
			String query = "SELECT u FROM Usuario u where u.usuario = ?1 and u.password = ?2";
			q = manager.createQuery(query);
			q.setParameter(1, username);
			q.setParameter(2, password);
			List<Usuario> usuarios = (List<Usuario>) q.getResultList();
			if (!usuarios.isEmpty()) {
				usuario = usuarios.get(0);
			}				
		} catch (Exception e) {
			e.printStackTrace();
		} 		
		return usuario;
	}

}
