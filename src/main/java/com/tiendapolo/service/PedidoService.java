package com.tiendapolo.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.tiendapolo.entidad.Pedido;
import com.tiendapolo.entidad.PedidoHasProducto;

public class PedidoService {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("tiendapolos");
	EntityManager manager = emf.createEntityManager();


	@SuppressWarnings("unchecked")
	public List<Pedido> pedidoXusuario(int idUsuario) {
		List<Pedido> pedidos = new ArrayList<Pedido>();		
		try {
			Query q = null;
			String query = "SELECT u FROM Pedido u where u.usuario.idUsuario = ?1";
			q = manager.createQuery(query);
			q.setParameter(1, idUsuario);
			pedidos = (List<Pedido>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return pedidos;
		
	}


	@SuppressWarnings("unchecked")
	public List<PedidoHasProducto> detallepedidoXpedido(int idPedido) {
		List<PedidoHasProducto> pedidos = new ArrayList<PedidoHasProducto>();		
		try {
			Query q = null;
			String query = "SELECT u FROM PedidoHasProducto u where u.pedido.idPedido = ?1";
			q = manager.createQuery(query);
			q.setParameter(1, idPedido);
			pedidos = (List<PedidoHasProducto>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return pedidos;
		
	}

	public boolean registrar(Pedido pedido) {
		boolean guardado = false;
		//GOOD cause readable
		EntityTransaction tx = manager.getTransaction();
		try {
			tx.begin();
			manager.persist(pedido);
			tx.commit();
			guardado = true;
		} catch (Exception e) {
			tx.rollback();
		}
		return guardado;
	}
	
	public boolean registrarDetalle(PedidoHasProducto pHprod) {
		boolean guardado = false;

		//GOOD cause readable
		EntityTransaction tx = manager.getTransaction();
		try {
			tx.begin();
			manager.persist(pHprod);
			tx.commit();
			guardado = true;
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
		return guardado;
	}
	
}
