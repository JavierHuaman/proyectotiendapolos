package com.tiendapolo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tiendapolo.entidad.Pedido;
import com.tiendapolo.entidad.PedidoHasProducto;
import com.tiendapolo.entidad.Producto;
import com.tiendapolo.entidad.ProductoHasProducto;
import com.tiendapolo.entidad.Usuario;
import com.tiendapolo.service.PedidoService;
import com.tiendapolo.service.PoloService;

/**
 * Servlet implementation class PedidoServlet
 */
public class PedidoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PoloService poloService = new PoloService();
	private PedidoService pedidoService = new PedidoService();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		if (usuario == null) {
			response.sendRedirect("/tiendapolo/carritoCompras.jsp");
		} else {
			List<Producto> productos = (List<Producto>) request.getSession().getAttribute("productos");
			double total = (Double) request.getSession().getAttribute("total");
			Pedido pedido = new Pedido();
			pedido.setFecha(new Date());
			pedido.setUsuario(usuario);
			pedido.setTotal(total);
			pedidoService.registrar(pedido);
			List<PedidoHasProducto> pHprods = new ArrayList<PedidoHasProducto>();
			
			for (Producto polo : productos) {
				poloService.registrar(polo);				
				PedidoHasProducto pHprod = new PedidoHasProducto();
				pHprod.setProducto(polo);
				pHprod.setPedido(pedido);
				pHprods.add(pHprod);
			}
			
			for (Producto polo : productos) {
				for (Producto artWork : polo.getProductos()) {
					ProductoHasProducto pHp = new ProductoHasProducto();
					pHp.setProducto1(polo);
					pHp.setProducto2(artWork);
					poloService.registrarDetalleProducto(pHp);
				}
			}
			
			for (PedidoHasProducto pedidoHasProducto : pHprods) {
				pedidoService.registrarDetalle(pedidoHasProducto);
			}

			request.getSession().removeAttribute("total");
			request.getSession().removeAttribute("productos");
			response.sendRedirect("/tiendapolo/perfil.jsp");
			
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
