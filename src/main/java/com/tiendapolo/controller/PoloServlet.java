package com.tiendapolo.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tiendapolo.entidad.Producto;
import com.tiendapolo.service.PoloService;

/**
 * Servlet implementation class PoloServlet
 */
public class PoloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private PoloService poloService = new PoloService();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Producto> polos = poloService.productoXtipo(1); // Trae todos los polos
		List<Producto> artworks = poloService.productoXtipo(2); // Trae todos los diseños

		request.setAttribute("polos", polos);
		request.setAttribute("artworks", artworks);
		request.getRequestDispatcher("polos.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
