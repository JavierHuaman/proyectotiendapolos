package com.tiendapolo.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tiendapolo.entidad.Usuario;
import com.tiendapolo.service.UsuarioService;

/**
 * Servlet implementation class UsuarioServlet
 */
public class UsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UsuarioService usuarioService = new UsuarioService();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombres = request.getParameter("nombres");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		String username = request.getParameter("usuario");
		String password = request.getParameter("password");
		
		Usuario usuario = new Usuario();
		usuario.setEmail(email);
		usuario.setNombres(nombres);
		usuario.setPassword(password);
		usuario.setTelefono(telefono);
		usuario.setUsuario(username);
		
		if (usuarioService.registrar(usuario)) {
			request.getSession().setAttribute("usuario", usuario);
			response.sendRedirect("/tiendapolo/polos");			
		} else {
			response.sendRedirect("/tiendapolo/signup.jsp");			
		}
		
		
	}

}
