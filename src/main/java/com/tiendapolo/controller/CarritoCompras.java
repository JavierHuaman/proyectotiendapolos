package com.tiendapolo.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import com.tiendapolo.entidad.Producto;
import com.tiendapolo.service.PoloService;

/**
 * Servlet implementation class CarritoCompras
 */
public class CarritoCompras extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private PoloService poloService = new PoloService();
	private String UPLOAD_DIRECTORY = "upload";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		if (operacion.equals("eliminar")) {
			int idProd = Integer.parseInt(request.getParameter("idProd"));

			List<Producto> productos = (List<Producto>) request.getSession().getAttribute("productos");
			double total = (Double) request.getSession().getAttribute("total");
			
			if (productos != null) {
				for (int i = 0; i < productos.size(); i++) {
					if (productos.get(i).getIdProducto() == idProd) {
						File file = new File(getServletContext().getRealPath("") + File.separator + 
								   UPLOAD_DIRECTORY + File.separator + productos.get(i).getImagen());
						file.delete();
						total -= productos.get(i).getPrecio();
						productos.remove(i);
						break;
					}				
				}
				request.getSession().setAttribute("productos", productos);
				request.getSession().setAttribute("total", total);
			}
			response.sendRedirect("/tiendapolo/carritoCompras.jsp");
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		Producto prod = new Producto();
		prod.setIdProducto(Integer.parseInt(request.getParameter("idProducto")));
		prod.setDetalle(request.getParameter("detalle"));
		prod.setDescripcion(request.getParameter("descripcion"));
		prod.setColores(request.getParameter("colores"));
		prod.setPrecio(Double.parseDouble(request.getParameter("precio")));
		
		List<Producto> det = new ArrayList<Producto>();
		
		for (String dtl : prod.getDetalle().split(";")) {
			det.add(poloService.buscarXid(Integer.parseInt(dtl)));
		}

		prod.setProductos(det); 
		
		List<Producto> productos = (List<Producto>) request.getSession().getAttribute("productos");
		double total = 0;
		
		if (request.getSession().getAttribute("total") != null) {
			total = (Double) request.getSession().getAttribute("total");
		}	 
		
		
		if (productos == null) {
			productos = new ArrayList<Producto>();
		}
		
		if (operacion.equals("agregar")) {
			String imgdata = request.getParameter("imgdata");
			InputStream is = new ByteArrayInputStream(Base64.decodeBase64(imgdata));
			String nombreArchivo = UUID.randomUUID().toString().substring(0, 8) + ".png";
			File ff = new File(getServletContext().getRealPath("") + File.separator + 
												   UPLOAD_DIRECTORY);
			if (!ff.exists()) {
				ff.mkdir();
			}
			
			OutputStream os = new FileOutputStream(getServletContext().getRealPath("") + File.separator + 
												   UPLOAD_DIRECTORY + File.separator + nombreArchivo);
	        
	        byte[] buffer = new byte[1024];
	        int bytesRead;
	        //read from is to buffer
	        while((bytesRead = is.read(buffer)) !=-1){
	            os.write(buffer, 0, bytesRead);
	        }
	        is.close();
	        //flush OutputStream to write any buffered data to file
	        os.flush();
	        os.close();
			prod.setImagen(nombreArchivo);
			productos.add(prod);
			total += prod.getPrecio();
		}
		request.getSession().setAttribute("total", total);
		request.getSession().setAttribute("productos", productos);		
	}

}
