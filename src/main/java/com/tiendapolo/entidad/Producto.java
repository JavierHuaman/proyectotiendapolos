package com.tiendapolo.entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Producto database table.
 * 
 */
@Entity
@Table(name="Producto")
@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idProducto;

	@Lob
	private String colores;

	private String descripcion;

	@Lob
	private String imagen;

	private double precio;

	//bi-directional many-to-one association to PedidoHasProducto
	@OneToMany(mappedBy="producto")
	private List<PedidoHasProducto> pedidoHasProductos;

	//bi-directional many-to-one association to TipoProducto
	@ManyToOne
	@JoinColumn(name="idTipoProducto")
	private TipoProducto tipoProducto;

	//bi-directional many-to-one association to ProductoHasProducto
	@OneToMany(mappedBy="producto1")
	private List<ProductoHasProducto> productoHasProductos1;

	//bi-directional many-to-one association to ProductoHasProducto
	@OneToMany(mappedBy="producto2")
	private List<ProductoHasProducto> productoHasProductos2;
	
	@Transient
	private String detalle;
	
	@Transient
	private List<Producto> productos;

	public Producto() {
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getColores() {
		return this.colores;
	}

	public void setColores(String colores) {
		this.colores = colores;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return this.imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public double getPrecio() {
		return this.precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public List<PedidoHasProducto> getPedidoHasProductos() {
		return this.pedidoHasProductos;
	}

	public void setPedidoHasProductos(List<PedidoHasProducto> pedidoHasProductos) {
		this.pedidoHasProductos = pedidoHasProductos;
	}

	public PedidoHasProducto addPedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().add(pedidoHasProducto);
		pedidoHasProducto.setProducto(this);

		return pedidoHasProducto;
	}

	public PedidoHasProducto removePedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().remove(pedidoHasProducto);
		pedidoHasProducto.setProducto(null);

		return pedidoHasProducto;
	}

	public TipoProducto getTipoProducto() {
		return this.tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public List<ProductoHasProducto> getProductoHasProductos1() {
		return this.productoHasProductos1;
	}

	public void setProductoHasProductos1(List<ProductoHasProducto> productoHasProductos1) {
		this.productoHasProductos1 = productoHasProductos1;
	}

	public ProductoHasProducto addProductoHasProductos1(ProductoHasProducto productoHasProductos1) {
		getProductoHasProductos1().add(productoHasProductos1);
		productoHasProductos1.setProducto1(this);

		return productoHasProductos1;
	}

	public ProductoHasProducto removeProductoHasProductos1(ProductoHasProducto productoHasProductos1) {
		getProductoHasProductos1().remove(productoHasProductos1);
		productoHasProductos1.setProducto1(null);

		return productoHasProductos1;
	}

	public List<ProductoHasProducto> getProductoHasProductos2() {
		return this.productoHasProductos2;
	}

	public void setProductoHasProductos2(List<ProductoHasProducto> productoHasProductos2) {
		this.productoHasProductos2 = productoHasProductos2;
	}

	public ProductoHasProducto addProductoHasProductos2(ProductoHasProducto productoHasProductos2) {
		getProductoHasProductos2().add(productoHasProductos2);
		productoHasProductos2.setProducto2(this);

		return productoHasProductos2;
	}

	public ProductoHasProducto removeProductoHasProductos2(ProductoHasProducto productoHasProductos2) {
		getProductoHasProductos2().remove(productoHasProductos2);
		productoHasProductos2.setProducto2(null);

		return productoHasProductos2;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

}