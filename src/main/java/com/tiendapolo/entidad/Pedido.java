package com.tiendapolo.entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Pedido database table.
 * 
 */
@Entity
@Table(name="Pedido")
@NamedQuery(name="Pedido.findAll", query="SELECT p FROM Pedido p")
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPedido;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private double total;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;

	//bi-directional many-to-one association to PedidoHasProducto
	@OneToMany(mappedBy="pedido")
	private List<PedidoHasProducto> pedidoHasProductos;

	public Pedido() {
	}

	public int getIdPedido() {
		return this.idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<PedidoHasProducto> getPedidoHasProductos() {
		return this.pedidoHasProductos;
	}

	public void setPedidoHasProductos(List<PedidoHasProducto> pedidoHasProductos) {
		this.pedidoHasProductos = pedidoHasProductos;
	}

	public PedidoHasProducto addPedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().add(pedidoHasProducto);
		pedidoHasProducto.setPedido(this);

		return pedidoHasProducto;
	}

	public PedidoHasProducto removePedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().remove(pedidoHasProducto);
		pedidoHasProducto.setPedido(null);

		return pedidoHasProducto;
	}

}