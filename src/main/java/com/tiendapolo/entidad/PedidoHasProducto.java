package com.tiendapolo.entidad;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Pedido_has_Producto database table.
 * 
 */
@Entity
@Table(name="Pedido_has_Producto")
@NamedQuery(name="PedidoHasProducto.findAll", query="SELECT p FROM PedidoHasProducto p")
public class PedidoHasProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPedidoProducto;

	//bi-directional many-to-one association to Pedido
	@ManyToOne
	@JoinColumn(name="idPedido")
	private Pedido pedido;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto producto;

	public PedidoHasProducto() {
	}

	public int getIdPedidoProducto() {
		return this.idPedidoProducto;
	}

	public void setIdPedidoProducto(int idPedidoProducto) {
		this.idPedidoProducto = idPedidoProducto;
	}

	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}