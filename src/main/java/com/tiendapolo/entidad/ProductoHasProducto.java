package com.tiendapolo.entidad;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Producto_has_Producto database table.
 * 
 */
@Entity
@Table(name="Producto_has_Producto")
@NamedQuery(name="ProductoHasProducto.findAll", query="SELECT p FROM ProductoHasProducto p")
public class ProductoHasProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idDetalle;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto producto1;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="idProducto1")
	private Producto producto2;

	public ProductoHasProducto() {
	}

	public int getIdDetalle() {
		return this.idDetalle;
	}

	public void setIdDetalle(int idDetalle) {
		this.idDetalle = idDetalle;
	}

	public Producto getProducto1() {
		return this.producto1;
	}

	public void setProducto1(Producto producto1) {
		this.producto1 = producto1;
	}

	public Producto getProducto2() {
		return this.producto2;
	}

	public void setProducto2(Producto producto2) {
		this.producto2 = producto2;
	}

}